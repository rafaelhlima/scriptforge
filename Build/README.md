# I.		LOAD NEW SCRIPTFORGE FILES

## 	1.	Copy next libraries from ~/.config/libreoffice/4/user/basic to wizards/source
		Rename to lower case
		
		Git:	git checkout -b SF
				... copy files into the source tree
				git add wizard/source/DIR
				git commit -a
				git checkout master
				git pull -r
				git checkout SF
				git fetch origin && git rebase master && git push origin SF:master
				git checkout master
				git pull -r
				git branch -D SF
		Check on https://git.libreoffice.org/core/+log/HEAD
	
###		a)	ScriptForge - Commit 09c1bee1f913
        new file:   wizards/source/scriptforge/SF_Array.xba
        new file:   wizards/source/scriptforge/SF_Dictionary.xba
        new file:   wizards/source/scriptforge/SF_Exception.xba
        new file:   wizards/source/scriptforge/SF_FileSystem.xba
        new file:   wizards/source/scriptforge/SF_L10N.xba
        new file:   wizards/source/scriptforge/SF_Platform.xba
        new file:   wizards/source/scriptforge/SF_Root.xba
        new file:   wizards/source/scriptforge/SF_Services.xba
        new file:   wizards/source/scriptforge/SF_Session.xba
        new file:   wizards/source/scriptforge/SF_String.xba
        new file:   wizards/source/scriptforge/SF_TextStream.xba
        new file:   wizards/source/scriptforge/SF_Timer.xba
        new file:   wizards/source/scriptforge/SF_UI.xba
        new file:   wizards/source/scriptforge/SF_Utils.xba
        new file:   wizards/source/scriptforge/_CodingConventions.xba
        new file:   wizards/source/scriptforge/_ModuleModel.xba
        new file:   wizards/source/scriptfoe58d6a92609brge/__License.xba
        new file:   wizards/source/scriptforge/dialog.xlb
        new file:   wizards/source/scriptforge/dlgConsole.xdl
        new file:   wizards/source/scriptforge/dlgProgress.xdl
        new file:   wizards/source/scriptforge/script.xlb

###		b)	SFDatabases - Commit 584e32d7c776
        new file:   wizards/source/sfdatabases/SF_Database.xba
        new file:   wizards/source/sfdatabases/SF_Register.xba
        new file:   wizards/source/sfdatabases/__License.xba
        new file:   wizards/source/sfdatabases/dialog.xlb
        new file:   wizards/source/sfdatabases/script.xlb
		
###		c)	SFDialogs - Commit 9597440731ca
        new file:   wizards/source/sfdialogs/SF_Dialog.xba
        new file:   wizards/source/sfdialogs/SF_DialogControl.xba
        new file:   wizards/source/sfdialogs/SF_Register.xba
        new file:   wizards/source/sfdialogs/__License.xba
        new file:   wizards/source/sfdialogs/dialog.xlb
        new file:   wizards/source/sfdialogs/script.xlb
		
###		d)	SFDocuments - Commit cdedc00ff579
        new file:   wizards/source/sfdocuments/SF_Base.xba
        new file:   wizards/source/sfdocuments/SF_Calc.xba
        new file:   wizards/source/sfdocuments/SF_Document.xba
        new file:   wizards/source/sfdocuments/SF_Register.xba
        new file:   wizards/source/sfdocuments/__License.xba
        new file:   wizards/source/sfdocuments/dialog.xlb
        new file:   wizards/source/sfdocuments/script.xlb
		
##	2.	Python helper
	
###		a)	Create directory wizards/source/scriptforge/python
		
###		b)	Copy ScriptForgeHelper.py in wizards/source/scriptforge/python - Commit 0c69cc232a86
        new file:   wizards/source/scriptforge/python/ScriptForgeHelper.py
		
##	3.	PO files

###		a)	Create directory wizards/source/scriptforge/po
		
###		b)	Copy en.po and ScriptForge.pot in wizards/source/scriptforge/po - Commit d6a127f84250
        new file:   wizards/source/scriptforge/po/ScriptForge.pot
        new file:   wizards/source/scriptforge/po/en.po

#II.		LOAD NEW .MK FILES

###		a)	wizards/Package_scriptforge.mk
	
###		b)	wizards/Package_sfdatabases.mk
	
###		c)	wizards/Package_sfdialogs.mk
	
###		d)	wizards/Package_sfdocuments.mk - Commit e58d6a92609b
        new file:   wizards/Package_scriptforge.mk
        new file:   wizards/Package_sfdatabases.mk
        new file:   wizards/Package_sfdialogs.mk
        new file:   wizards/Package_sfdocuments.mk

#III.	BUILD LOCAL LO (MASTER)

#IV.		UPDATE EXISTING FILES (SF)

##	1.	a) wizards/source/configshare/dialog.xlc
 <library:library library:name="ScriptForge" xlink:href="$(INST)/@LIBO_SHARE_FOLDER@/basic/ScriptForge/dialog.xlb/" xlink:type="simple" library:link="true" library:readonly="false"/>
 <library:library library:name="SFDatabases" xlink:href="$(INST)/@LIBO_SHARE_FOLDER@/basic/SFDatabases/dialog.xlb/" xlink:type="simple" library:link="true" library:readonly="false"/>
 <library:library library:name="SFDialogs" xlink:href="$(INST)/@LIBO_SHARE_FOLDER@/basic/SFDialogs/dialog.xlb/" xlink:type="simple" library:link="true" library:readonly="false"/>
 <library:library library:name="SFDocuments" xlink:href="$(INST)/@LIBO_SHARE_FOLDER@/basic/SFDocuments/dialog.xlb/" xlink:type="simple" library:link="true" library:readonly="false"/>

##	1.	b) wizards/source/configshare/script.xlc
 <library:library library:name="ScriptForge" xlink:href="$(INST)/@LIBO_SHARE_FOLDER@/basic/ScriptForge/script.xlb/" xlink:type="simple" library:link="true" library:readonly="false"/>
 <library:library library:name="SFDatabases" xlink:href="$(INST)/@LIBO_SHARE_FOLDER@/basic/SFDatabases/script.xlb/" xlink:type="simple" library:link="true" library:readonly="false"/>
 <library:library library:name="SFDialogs" xlink:href="$(INST)/@LIBO_SHARE_FOLDER@/basic/SFDialogs/script.xlb/" xlink:type="simple" library:link="true" library:readonly="false"/>
 <library:library library:name="SFDocuments" xlink:href="$(INST)/@LIBO_SHARE_FOLDER@/basic/SFDocuments/script.xlb/" xlink:type="simple" library:link="true" library:readonly="false"/>
	
##	2.	Repository.mk (+/- line 930)
		desktop/CppunitTest_desktop_lib.mk (+/- line 60) - Check alignments with tabs/spaces
	wizards_basicsrvscriptforge \
	wizards_basicsrvsfdatabases \
	wizards_basicsrvsfdialogs \
	wizards_basicsrvsfdocuments \
	
##	3.	scp2/source/ooo/directory_ooo.scp (+/- line 250)
Directory gid_Dir_Basic_ScriptForge
    ParentID = gid_Dir_Basic;
    DosName = "ScriptForge";
End

Directory gid_Dir_Basic_SFDatabases
    ParentID = gid_Dir_Basic;
    DosName = "SFDatabases";
End

Directory gid_Dir_Basic_SFDialogs
    ParentID = gid_Dir_Basic;
    DosName = "SFDialogs";
End

Directory gid_Dir_Basic_SFDocuments
    ParentID = gid_Dir_Basic;
    DosName = "SFDocuments";
End
	
##	4.	wizards/Module_wizards.mk (+/- line 30)
	Package_scriptforge \
	Package_sfdatabases \
	Package_sfdialogs \
	Package_sfdocuments \

        modified:   Repository.mk
        modified:   desktop/CppunitTest_desktop_lib.mk
        modified:   scp2/source/ooo/directory_ooo.scp
        modified:   wizards/Module_wizards.mk
        modified:   wizards/source/configshare/dialog.xlc
        modified:   wizards/source/configshare/script.xlc

#V.		FINAL PUSH

##	1.	BUILD LOCAL LO (SF)
	
##	2.	Commit 0e75b9a863fd
	
##	3.	./logerrit submit master
        (read https://wiki.documentfoundation.org/Development/gerrit/SubmitPatch)
		Review patch on https://gerrit.libreoffice.org/c/core/+/105410
		
