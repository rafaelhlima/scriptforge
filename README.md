# ScriptForge
## Purpose
Build an extensible and robust collection of macro scripting resources for LibreOffice
to be invoked from user Basic or Python scripts.
## Context
This project comes with a bunch of subprogram (“primitives”) aimed at filling some programming gaps in LibreOffice Basic. These primitives cover many of LibreOffice functionalities and are meant to be extended by any contributor over time. They complement - and often replace - the usual `Tools` library made available under LibreOffice packages since the OpenOffice years.
## Integration into LibreOffice
The development of the software is done currently with LibreOffice 7.0. Tests are undergone both under the Windows and Linux operating systems.  

When ready, the code will be integrated into the master branch of Libreoffice. It will appear as one or more additional "**LibreOffice Macros & Dialogs**" libraries.  

The intent is to have the software ready for production for the next **LibreOffice 7.1** release.  

Any pre-existing Basic or Python library in the LibreOffice software is left unchanged.
## Deliverables (1st version 7.1)
- A set of Basic + Python modules and classes covering next topics
     - about data containers
          + an extensive set of primitives for array handling, including sorts, set operations and interfaces with csv files and Calc sheets
          + an extensive set of primitives for string handling, including replacements, regular expressions,  encodings, hashing and localization
          + a Dictionary mapping class
     - a coherent error handling for both user and ScriptForge scripts
     - complete FileSystem and TextStream classes for file and directory handling, and text files read & write
     - detailed context information: platform, operating system, directories, ...
     - interconnection of Basic and Python modules
     - easy access to and management of actual windows  and documents
     - specific modules for
           + automation on Calc sheets
           + Dialogs and their controls
           + Toolbars and their controls
           + Accessing data contained in databases
- The corresponding unit tests grouped in a QA library
- The user documentation as a set of LibreOffice Help pages
## Extensibility
The described modules and classes will be invoked from user scripts as "Services". A generic constructor of those services has been designed for that purpose:
```
    CreateScriptService("servicename"[, arg0, Arg1, ...])
```
Symmetrically services are registered with the `RegisterScriptServices(...)` method. This method may be invoked from external libraries to extend the set of available services. 
 
Above mechanisms make ScriptForge an easily extensible ecosystem combining standard libraries, libraries published as extensions or distributed as company extensions.