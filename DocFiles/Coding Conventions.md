This page details the conventions used in the coding of the *ScriptForge* API.
## Library and Modules
* Module names are all prefixed with "SF_".
    * The *Option Explicit* statement is mandatory in every module.
    * The *Option Private Module* statement is recommended in internal modules.
* A standard header presenting the module/class is mandatory
* A end of file (eof) comment line is mandatory
* Every module lists the constants that are related to it and documented as return values, arguments, etc. They are defined as *Global*. The scope of global constants being limited to one single library, their invocation from user scripts shall be qualified.
* The Basic reserved words are *Proper-Cased*.
## Functions and Subroutines
* LibreOffice ignores the Private/Public attribute in Functions or Subs declarations. Nevertheless the attribute must be present. Rules for use are:
    * Public and name starts with a letter  
        * The Sub/Function belongs to the ScriptForge API. As such it may be called from any user script.
    * Public and name starts with an underscore "_"
        * The Sub/Function may be called only from within the ScriptForge library. As such it MUST NOT be called from another library or from a user script as there is no guarantee about the arguments, the semantic or even the existence of that piece of code in a later release.
    * Private - The Sub/Function name must start with an underscore "_".  
        * The Sub/Function must be called only from the module in which it is located.
* Functions and Subroutines belonging to the API (= "standard" functions/Subs) are defined in their module in alphabetical order. For class modules, all the properties precede the methods which precede the events.
* Functions and Subroutines not belonging to the API are defined in their module in alphabetical order below the standard ones.
* The return value of a function is always declared explicitly.
* The parameters are always declared explicitly even if they're variants.
* The Function and Sub declarations start at the 1st column of the line.
* The End Function/Sub statement is followed by a comment reminding the name of the containing library.module and of the function or sub. If the Function/Sub is declared for the first time or modified in a release > initial public release, the actual release number is mentioned as well.
## Variable declarations
* Variable names use only alpha characters, the undercore and digits (no accented characters). Exceptionally, names of private variables may be embraced with `[` and `]` if `Option Compatible` is present.
* The Global, Dim and Const statements always start in the first column of the line.
* The type (*Dim ... As ...*, *Function ... As ...*) is always declared explicitly, even if the type is Variant.
* Variables are *Proper-Cased*. They are always preceded by a lower-case letter indicating their type. With next exception: variables i, j, k, l, m and n must be declared as integers or longs.
> b Boolean  
> d Date  
> v Variant  
> o Object  
> i Integer  
> l Long  
> s String  
Example:  
    `Dim sValue As String`
* Parameters are preceded by the letter *p* which itself precedes the single *typing letter*. For official methods, to match their published documentation, the *p* and the *typing letter* may be omitted. Like in:  
```
    Private Function MyFunction(psValue As String) As Variant
    Public Function MyOfficialFunction(Value As String) As Variant
```
* Global variables in the ScriptForge library are ALL preceded by an underscore "_" as NONE of them should be invoked from outside the library.
* Constant values with a local scope are *Proper-Cased* and preceded by the letters *cst*.
* Constants with a global scope are *UPPER-CASED*.  
Example:
``` 
    Global Const ACONSTANT = "This is a global constant"  
    Function MyFunction(pocControl As Object, piValue) As Variant  
    Dim iValue As Integer  
    Const cstMyConstant = 3 
``` 
## Indentation
Code shall be indented with TAB characters.
## Goto
The *GoTo* statement is forbidden.  
It is however highly recommended for __error__ and __exception__ handling.
## Comments (english only)
* Every public routine should be documented with a python-like "docstring":
    1. Role of Sub/Function
    2. List of arguments, mandatory/optional, role
    3. Returned value(s) type and meaning
    4. Examples if necessary 
    5. Eventual specific error codes
* The "docstring" comments shall be marked by a triple (single) quote character at the beginning of the line
* Meaningful variables shall be declared one per line. Comment on same line.
* Comments about a code block should be left indented. If it concerns only the next line, no indent required (may also be put at the end of the line).
## Typical Sub/Function/Method skeleton
```
REM ===============================================================================================
Public Function myFunction(Optional ByVal Arg1 As Variant, Optional ByVal Arg2 As Variant) As Variant
''' Role: concatenates Arg1 Arg2 times
' Arguments:
'   Arg1            String      Text
'   Arg2            Numeric     Number of times (default = 2)
' Returns:
'   The new string  String
' Exceptions:
'   SF_ServiceDoesNotExistError
'   PythonScriptingFrameworkIsAbsentException
' Examples:
'   result = myFunction("value1")
'   v = MyFunction(parm1, 34)
'''
Dim sOutput As String       '   Output buffer
Dim i As Integer
Const cstThisSub = "SF_Module.myFunction"
Const cstSubArgs = "Arg1, [Arg2 = 2]"

    '   A dev might prefer for debugging to switch on standard error handling
    If SF_Utils._ErrorHandling() Then On Local Error GoTo Catch

    myFunction = ""
Check:
    If IsMissing(Arg2) Then Arg2 = 2
    If SF_Utils._EnterFunction(cstThisSub, cstSubArgs) Then      '   Avoid (re)checks when internal call
        If Not SF_Utils._Validate(Arg1, "Arg1", V_STRING) Then GoTo Finally
        If Not SF_Utils._Validate(Arg1, "Arg2", V_NUMERIC) Then GoTo Finally
    End If

Try:
    sOutput = ""
    For i = 0 To Arg2
        sOutput = sOutput & Arg1
    Next i
    myFunction = sOutput
Finally:
    SF_Utils._ExitFunction(cstThisSub)
    Exit Function
Catch:
    SF_Exception.RaiseAbort(Err, cstThisSub, Erl)
    GoTo Finally
End Function    '   ThisLibrary.ThisModule.myFunction V2.0
```