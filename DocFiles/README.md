All text and image content in the current directory and subdirectories is licensed under the Creative Commons Attribution-Share Alike 4.0 License **(unless otherwise specified)**.

“LibreOffice” and “The Document Foundation” are registered trademarks. Their respective logos and icons are subject to international copyright laws. The use of these thereof is subject to trademark policy.