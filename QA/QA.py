import sys
import uno
import platform

if platform.system() == 'Linux':
    sys.path.append('/home/jean-pierre/.config/libreoffice/4/user/Scripts/python/QA')
else:
    sys.path.append(r'C:\Users\jp\AppData\Roaming\LibreOffice\4\user\Scripts\python\QA')

from scriptforge import ScriptForge, CreateScriptService

if __name__ == '__main__':
    # Launch LibreOffice with /opt/libreoffice7.1/program/./soffice --accept='socket,host=localhost,port=2021;urp;'
    ScriptForge('localhost', 2021)

userinteraction = False
shellconsole = False
timer = None
exc = CreateScriptService('exception')
bas = CreateScriptService('basic')
ui = CreateScriptService('ui')
statuscount = 0

def main():
    global timer
    setup()
    timer = CreateScriptService('timer')
    timer.start()

    testArray()
    testBasic()
    testDictionary()
    testException()
    testFileSystem()
    testL10N()
    testPlatform()
    testSession()
    testString()
    testTextStream()
    testTimer()
    testUI()
    testDatabase()
    testDocument()
    testCalc()
    testForm()
    testDialog()

    timer.terminate()
    printprogress('TIMER:', timer.Duration)
    teardown()

def printprogress(*args):
    global statuscount
    cstSteps = 1000
    d = timer.duration
    exc.debugPrint(d, *args)
    statuscount += 1
    ui.ShowProgressBar('Progress', str(statuscount) + '. ' + str(args[0]), statuscount / cstSteps * 100)
    ui.SetStatusbar(str(statuscount) + '. ' + str(args[0]), statuscount / cstSteps * 100)
    if shellconsole:
        print(d, *args)

def setup():
    global exc, bas, ui
    # ScriptForge('localhost', 2021)
    ScriptForge.InvokeSimpleScript('ScriptForge.SF_Utils._InitializeRoot', True)
    # Delete temporary files from previous runs
    FSO = CreateScriptService('FileSystem')
    if len(FSO.SubFolders(FSO.TemporaryFolder, 'SF_*')) > 0:
        FSO.DeleteFolder(FSO.BuildPath(FSO.TemporaryFolder, 'SF_*'))
    # Start console registration
    exc.Console(False)
    if shellconsole:
        exc.PythonShell({**globals(), **locals()})
    bas = CreateScriptService('basic')
    ui = CreateScriptService('ui')

def teardown():
    ui.SetStatusbar()


# #####################################################################################################################
# TEST ARRAY                                                                                                        ###
#######################################################################################################################
def testArray():
    import datetime
    printprogress('TEST ARRAY')
    array = CreateScriptService('array')
    FSO = CreateScriptService("FileSystem")
    FSO.FileNaming = "SYS"
    a = FSO.GetTempName()
    b = FSO.BuildPath(a, "myCSVfile.csv")
    file = FSO.CreateTextFile(b, overwrite = True)
    file.WriteLine(
        "policyID,statecode,county,eq_site_limit,hu_site_limit,fl_site_limit,fr_site_limit,tiv_2011,tiv_2012,eq_site_deductible,hu_site_deductible,fl_site_deductible,fr_site_deductible,point_latitude,point_longitude,line,construction,point_granularity")
    file.WriteLine(
        "119736,FL,CLAY COUNTY,498960,498960,498960,498960,498960,792148.9,0,9979.2,0,0,30.102261,-81.711777,Residential,""Masonry"",1")
    file.WriteLine(
        "448094,FL,CLAY COUNTY,1322376.3,1322376.3,1322376.3,1322376.3,1322376.3,1438163.57,0,0,0,0,30.063936,-81.707664,Residential,Masonry,3")
    file.WriteLine(
        "206893,FL,CLAY COUNTY,190724.4,190724.4,190724.4,190724.4,190724.4,192476.78,0,0,0,0,30.089579,-81.700455,Residential,Wood,2020/03/28")
    file.WriteLine(
        "333743,FL,CLAY COUNTY,0,79520.76,0,0,79520.76,86854.48,0,0,0,0,30.063236,-81.707703,Residential,Wood,3")
    file.WriteLine(
        "172534,FL,CLAY COUNTY,0,254281.5,0,254281.5,254281.5,246144.49,0,0,0,0,30.060614,-81.702675,Residential,Wood,1")
    file.WriteLine(
        "785275,FL,CLAY COUNTY,0,515035.62,0,0,515035.62,884419.17,0,0,0,0,30.063236,-81.707703,Residential,Masonry,3")
    file.WriteLine(
        '995932,FL,CLAY COUNTY,0,19260000,0,0,19260000,20610000,0,0,0,0,30.102226,-81.713882,Commercial,"""Reinforced"", and Concrete",1')
    file.WriteLine(
        "223488,FL,CLAY COUNTY,328500,328500,328500,328500,328500,348374.25,0,16425,0,0,30.102217,-81.707146,Residential,Wood,1")
    file.WriteLine(
        "433512,FL,CLAY COUNTY,315000,315000,315000,315000,315000,265821.57,0,15750,0,0,30.118774,-81.704613,Residential,Wood,1")
    file.WriteLine(
        "142071,FL,CLAY COUNTY,705600,705600,705600,705600,705600,1010842.56,14112,35280,0,0,30.100628,-81.703751,Residential,Masonry,1")
    file.WriteLine(
        "253816,FL,CLAY COUNTY,831498.3,831498.3,831498.3,831498.3,831498.3,1117791.48,0,0,0,0,30.10216,-81.719444,Residential,Masonry,1")
    file.CloseFile()
    c = array.importFromCSVFile(b, delimiter = ",", dateformat = "YYYY/MM/DD")
    FSO.DeleteFolder(a)
    printprogress('Date:', c[3][17], datetime.date.fromisoformat(c[3][17]))
    assert datetime.date.fromisoformat(c[3][17]) == datetime.date(2020, 3, 28)
    assert c[1][16] == 'Masonry'
    printprogress('Quotes:', c[7][16])
    assert c[7][16] == '"Reinforced", and Concrete'
    printprogress('Last line:', c[-1][0])
    assert isinstance(c[-1][0], int) and c[-1][0] == 253816


# #####################################################################################################################
# TEST BASIC                                                                                                        ###
#######################################################################################################################
def testBasic():
    printprogress('TEST BASIC')
    bas = CreateScriptService('Basic')
    a = 'file:///home/jean-pierre/Documents/Access2Base/Doc/Access2Base/access2base.html'
    b = bas.ConvertFromUrl(a)
    c = bas.ConvertToUrl(b)
    assert a == c
    printprogress('CreateUnoService', bas.CreateUnoService('com.sun.star.i18n.CharacterClassification'))
    printprogress('Date functions')
    d = bas.CDate('abcd')
    printprogress('CDate', 'abcd', d)
    assert d == 'abcd'
    d = bas.CDate(1001.26)
    printprogress('CDate', 1001.26, d)
    assert d.second == 24
    d = bas.CDate('2021-02-18')
    printprogress('CDate', '2021-02-18', d)
    assert d.year == 2021
    d = bas.CDateToUnoDateTime(bas.now())
    e = bas.CDateFromUnoDateTime(d)
    assert abs(bas.datediff('m', d, bas.Now())) < 2
    assert bas.datediff('d', bas.dateAdd('d', 1, bas.now()), bas.Now()) == -1
    assert bas.DatePart('yyyy', bas.now()) > 2020
    assert bas.DatePart('yyyy', bas.DateValue('2021-02-18')) == 2021
    assert bas.Format(6328.2, '##,##0.00') == '6,328.20'
    printprogress('GetDefaultContext', bas.GetDefaultContext())
    printprogress('GetGuiType', bas.getGuiType())
    printprogress('GetSystemTicks', bas.GetSystemTicks())
    assert bas.GetPathSeparator() in ('/', '\\')
    printprogress('GlobalScope')
    printprogress(bas.GlobalScope.BasicLibraries())
    printprogress(bas.GlobalScope.DialogLibraries())
    printprogress(bas.StarDesktop)
    if userinteraction:
        printprogress(bas.InputBox('This is a message', xpostwips = 10, ypostwips = 10))
        printprogress(bas.MsgBox('Some text', bas.MB_ABORTRETRYIGNORE + bas.MB_ICONEXCLAMATION,
                                      'Some title'))
        bas.Xray(bas.CreateUnoService('com.sun.star.i18n.CharacterClassification'))

# #####################################################################################################################
# TEST DICTIONARY                                                                                                   ###
#######################################################################################################################
def testDictionary():
    import datetime
    printprogress('TEST DICTIONARY')
    d = dict(A = 1, B = (1, 2, 3, 4), C = 3)
    a = CreateScriptService('dictionary', d)
    printprogress('Preloaded dict', a)
    # a.update(d)
    now = bas.Now()
    for t in (datetime.datetime, datetime.date, datetime.time):
        if t == datetime.datetime:
            a['D'] = now
        elif t == datetime.date:
            a['D'] = now.date()
        elif t == datetime.date:
            a['D'] = now.time()
        a['D'] = now
        a['E'] = bas.createUnoService('com.sun.star.i18n.CharacterClassification')
        printprogress('Updated dict', t, a)
        assert len(a) == 5
        b = a.convertToPropertyValues()
        printprogress('Extracted PropertyValues', b)
        c = repr(a)
        a.clear()
        printprogress('Cleared dict', a)
        a.importFromPropertyValues(b)
        printprogress('Reimported PropertyValues', a)
        assert len(a) == 5
        assert c == repr(a)

# #####################################################################################################################
# TEST EXCEPTION                                                                                                    ###
#######################################################################################################################
def testException():
    printprogress('TEST EXCEPTION')
    # Nothing to do, everything is in printprogress etc.

# #####################################################################################################################
# TEST FILESYSTEM                                                                                                   ###
#######################################################################################################################
def testFileSystem():
    """
    FSO = SFScriptForge.SF_FileSystem(ScriptForge.servicesmodules['ScriptForge.FileSystem'], 'SF_FileSystem',
                                      classmodule = SFServices.moduleStandard)
        """
    FSO = CreateScriptService("FileSystem")
    printprogress('TEST FILESYSTEM')
    FSO.FileNaming = 'SYS'
    a = ('E:\ScriptForge\Help\sf_filesystem.xhp' if bas.GetGuiType() == 1
         else '/home/jean-pierre/Documents/ScriptForge/Doc/Help/sf_filesystem.xhp')
    temp = FSO.TemporaryFolder

    printprogress('PickFile')
    if userinteraction:
        b = FSO.PickFile(a, "OPEN", "xhp")
        printprogress(FSO.FileNaming, "File = ", b)

    printprogress('PickFolder')
    if userinteraction:
        b = FSO.PickFolder(FSO.GetParentFolderName(a))
        printprogress(FSO.FileNaming, "Folder = ", b)

    printprogress('FolderExists')
    assert FSO.FolderExists(a) is False
    printprogress('FileExists')
    assert FSO.fileexists(a) is True

    printprogress('Folder properties')
    printprogress('TemporaryFolder', FSO.FileNaming, FSO.TemporaryFolder)
    printprogress('HomeFolder', FSO.FileNaming, FSO.HomeFolder)
    printprogress('InstallFolder', FSO.FileNaming, FSO.InstallFolder)
    printprogress('ConfigFolder', FSO.FileNaming, FSO.configfolder)
    printprogress('ExtensionsFolder', FSO.FileNaming, FSO.ExtensionsFolder)
    printprogress('TemplatesFolder', FSO.FileNaming, FSO.TemplatesFolder)
    printprogress('UserTemplatesFolder', FSO.FileNaming, FSO.UserTemplatesFolder)

    b = FSO.GetTempName()
    printprogress('GettempName', b)
    printprogress(FSO.FileNaming, temp)
    assert b.startswith(temp + 'SF_')

    FSO.FileNaming = 'URL'
    c = FSO.BuildPath(bas.ConvertToUrl(b), name = "This is a file name.old.odt")
    printprogress('BuildPath', FSO.FileNaming, "Path =", c)
    assert c.endswith("This%20is%20a%20file%20name.old.odt")

    FSO.FileNaming = "SYS"
    d = FSO.Files(FSO.GetParentFolderName(a), "sf_*.*")
    printprogress('Files', FSO.FileNaming, d[0][-50:])
    printprogress('Files', FSO.FileNaming, d[-1][-50:])

    FSO.FileNaming = "SYS"
    c = FSO.TemporaryFolder
    FSO.CreateFolder(FSO.BuildPath(c, "SF_ANY"))
    d = FSO.SubFolders(c, "SF_*")
    printprogress('SubFolders', FSO.FileNaming, d[0])
    printprogress('SubFolders', FSO.FileNaming, d[-1], len(d) - 1)
    assert d[0].endswith(bas.GetPathSeparator())
    FSO.DeleteFolder(FSO.BuildPath(c, "SF_ANY"))

    FSO.FileNaming = "SYS"
    b = FSO.GetTempName()
    c = FSO.BuildPath(b, name = "This is a file name.old.odt")
    printprogress('GetName', FSO.FileNaming, FSO.GetName(c))
    assert FSO.GetName(c) == "This is a file name.old.odt"

    FSO.FileNaming = "SYS"
    b = FSO.GetTempName()
    c = FSO.BuildPath(b, name = "This is a file name.old.odt")
    printprogress('GetExtension', FSO.FileNaming, FSO.GetExtension(c))
    assert FSO.GetExtension(c) == "odt"

    FSO.FileNaming = "SYS"
    b = FSO.GetTempName()
    c = FSO.BuildPath(b, name = "This is a file name.old.odt")
    printprogress('GetBaseName', FSO.FileNaming, FSO.GetBaseName(c))
    assert FSO.GetBaseName(c) == "This is a file name.old"

    FSO.FileNaming = "URL"
    b = FSO.GetTempName()
    c = FSO.BuildPath(b, name = "This is a file name.old.odt")
    printprogress('GetParentFolderName', FSO.FileNaming, FSO.GetParentFolderName(c))
    assert '/SF_' in FSO.GetParentFolderName(c)

    FSO.FileNaming = "SYS"
    b = FSO.GetTempName()
    c = FSO.BuildPath(b, "SF1")
    FSO.CreateFolder(c)
    d = FSO.BuildPath(c, FSO.GetName(a))
    printprogress('CopyFile', a[-40:], d[-40:])
    FSO.CopyFile(a, d, False)
    assert FSO.FileExists(d)
    e = FSO.BuildPath(FSO.GetParentFolderName(a), "*.xhp")
    printprogress('CopyFile', e[-40:], c[-40:])
    FSO.CopyFile(e, c, True)
    assert FSO.FileExists(FSO.BuildPath(c, "sf_array.xhp"))

    f = FSO.BuildPath(b, "SF2")
    printprogress('CopyFolder', c, f)
    FSO.CopyFolder(c, f)
    assert FSO.FileExists(FSO.BuildPath(f, "sf_array.xhp"))
    g = FSO.BuildPath(b, "SF*")
    h = FSO.BuildPath(b, "SF3")
    printprogress('CopyFolder', g, h)
    FSO.CopyFolder(g, h)
    assert FSO.FileExists(FSO.BuildPath(FSO.BuildPath(FSO.BuildPath(b, "SF3"), "SF2"), "sf_array.xhp"))

    c = FSO.BuildPath(FSO.BuildPath(b, "SF3"), "SF1")
    d = FSO.BuildPath(c, "sf_array.xhp")
    e = FSO.BuildPath(c, "sf_array2.xhp")
    printprogress('MoveFile', d, e)
    FSO.MoveFile(d, e)
    assert FSO.FileExists(e)
    f = FSO.BuildPath(b, "SF1")
    g = FSO.BuildPath(f, "*.xhp")
    h = FSO.BuildPath(b, "SF4")
    printprogress('MoveFile', g, h)
    FSO.MoveFile(g, h)
    assert FSO.FileExists(FSO.BuildPath(h, "sf_array.xhp"))

    c = FSO.BuildPath(b, "SF4")
    d = FSO.BuildPath(FSO.BuildPath(b, "SF1"), "SF4")
    printprogress('MoveFolder', c, d)
    FSO.CopyFolder(c, d) if bas.GetGuiType() == 1 else FSO.MoveFolder(c, d)
    assert FSO.FolderExists(d)
    g = FSO.BuildPath(b, "SF3")
    c = FSO.BuildPath(g, "*")
    printprogress('MoveFolder', c, d)
    FSO.CopyFolder(c, d) if bas.GetGuiType() == 1 else FSO.MoveFolder(c, d)
    assert FSO.FileExists(
        FSO.BuildPath(FSO.BuildPath(FSO.BuildPath(FSO.BuildPath(b, "SF1"), "SF4"), "SF1"), "sf_array2.xhp"))

    c = FSO.GetFileModified(a)
    printprogress('GetFileModified', c)
    assert bas.DatePart('yyyy', c) >= 2020

    c = FSO.GetFileLen(a)
    printprogress('GetFileLen', c)
    assert c > 50000

    c = FSO.BuildPath(b, "SF5")
    FSO.CreateFolder(c)
    d = FSO.BuildPath(c, FSO.GetName(a))
    FSO.CopyFile(a, d, False)
    printprogress('CompareFiles', a[-40:], d[-40:])
    assert FSO.CompareFiles(a, d, False)
    assert FSO.CompareFiles(d, a, True)

    c = FSO.BuildPath(b, "SF1/SF4/SF2")
    d = FSO.BuildPath(c, "sf_array.xhp")
    printprogress('DeleteFile', d)
    FSO.DeleteFile(d)
    assert FSO.FileExists(d) is False
    d = FSO.BuildPath(c, "*.xhp")
    printprogress('DeleteFile', d)
    FSO.DeleteFile(d)
    assert len(FSO.Files(c)) == 0    #	Empty array

    c = FSO.BuildPath(b, "SF1/SF4/")
    d = FSO.BuildPath(c, "*")
    printprogress('DeleteFolder', d)
    FSO.DeleteFolder(d)
    assert FSO.FolderExists(FSO.BuildPath(c, "SF1")) is False
    printprogress('DeleteFolder', b)
    FSO.DeleteFolder(b)
    assert FSO.FolderExists(b) is False

    FSO.FileNaming = "ANY"
    c = FSO.BuildPath(b, "SF1/SF4/afile.txt")
    printprogress('FileNaming', c)
    # assert SF_String.IsUrl(c)

    c = ('C:\Program Files\LibreOffice\program\msformslo.dll' if bas.GetGuiType() == 1
         else '/opt/libreoffice6.4/program/libbootstraplo.so')
    for d in ("MD5", "SHA1", "SHA224", "SHA256", "SHA384", "SHA512"):
        e = FSO.HashFile(c, d)
        printprogress('HashFile', c, d, e)
        assert len(e) > 0 # and SF_String.IsHexDigit(e)

    for h in FSO.Properties():
        printprogress('GetProperty', h, FSO.GetProperty(h))

    FSO.dispose()
    printprogress('Dispose', FSO)


# #####################################################################################################################
# TEST L10N                                                                                                         ###
#######################################################################################################################
def testL10N():
    printprogress('TEST L10N')
    a = CreateScriptService("L10N")
    FSO = CreateScriptService("FileSystem")
    FSO.FileNaming = "SYS"
    b = FSO.GetTempName()   # Tempry dir

    printprogress('AddText')
    a.AddText('', "Add a very, very long text, even longer, I %1 even further, I'm not sure to have reached" +
              "the end but arrival gets closer. Will this sentence be wrapped in the output POT file ?")
    a.AddText("key1", "Add another text", "This is the comment\nspread over two lines\twith tabulation.")
    assert a.GetText("Key1") == "Add another text"

    printprogress('ExportToPOTFile')
    FSO.CreateFolder(b)
    a.ExportToPOTFile(FSO.BuildPath(b, "SF.pot"))
    a = a.dispose()

    c = (r'E:\ScriptForge\Test\fr.po' if bas.GetGuiType() == 1
         else '/home/jean-pierre/Documents/ScriptForge/Doc/libo_multilingual_v_0_7_3/PO/fr.po')
    FSO.CopyFile(c, FSO.BuildPath(b, "fr.po"))
    a = CreateScriptService("L10N", b, "fr") # This loads a quite long .po file
    d = a.GetText("RID_COMMON_START_21")
    printprogress('GetText', d)
    assert d.startswith(
            "L'assistant n'a pas pu être exécuté, car certains fichiers importants sont manquants.\\nPour rétablir les chemins")
    d = a._("RID_COMMON_START_21")
    printprogress('GetText', d)
    assert d.startswith("L'assistant n'a pas pu être exécuté, car certains fichiers importants sont manquants.\\nPour rétablir les chemins")

    printprogress('locale', a.locale)
    for h in a.Properties():
        printprogress('GetProperty', h, a.GetProperty(h))

    FSO.DeleteFolder(b) # PUT BREAKPOINT HERE TO SEE TEMPRY FOLDER CONTENT
    a = a.dispose()


# #####################################################################################################################
# TEST PLATFORM                                                                                                     ###
#######################################################################################################################
def testPlatform():
    printprogress('TEST PLATFORM')
    pf = CreateScriptService('Platform')
    printprogress(pf.architecture)
    printprogress(pf.computerName)
    printprogress(pf.CPUCount)
    printprogress(pf.CurrentUser)
    printprogress(pf.Locale)
    printprogress(pf.Machine)
    printprogress(pf.OfficeVersion)
    printprogress(pf.OSName)
    printprogress(pf.oSPlatform)
    printprogress(pf.OSRelease)
    printprogress(pf.OSVersion)
    printprogress(pf.Printers)
    printprogress(pf.Processor)
    printprogress(pf.PythonVersion)
    pf.dispose()


# #####################################################################################################################
# TEST SESSION                                                                                                      ###
# #####################################################################################################################
def testSession():
    printprogress('TEST STRING')
    a = CreateScriptService('Session')

    b = a.ExecuteBasicScript('', 'Access2Base.Application.ProductCode')
    printprogress('ExecuteBasicScript', b)
    assert b[:11] == 'Access2Base'
    b = a.ExecuteBasicScript(None, 'Access2Base.Application.Version')
    printprogress('ExecuteBasicScript', b)
    assert b[:11] == 'LibreOffice'
    b = a.ExecuteBasicScript('', 'Access2Base.Application.HtmlEncode',
                             'Amounts in €, not in £. Montants affichés en €, pas en £.')
    printprogress('ExecuteBasicScript', b)
    assert '&euro;' in b

    b = a.ExecuteCalcFunction('NOW')
    printprogress('ExecuteCalcFunction', 'NOW', b)
    b = a.ExecuteCalcFunction("AVERAGE", 1, 3, 5, 7)
    printprogress('ExecuteCalcFunction', 'AVERAGE', b)
    assert b == 4.0
    b = a.ExecuteCalcFunction("ARABIC", "MXIV")
    printprogress('ExecuteCalcFunction', 'ARABIC', b)
    assert b == 1014.0
    b = a.ExecuteCalcFunction("ABS", ((-1, 2, 3), (4, -5, 6), (7, 8, -9)))
    printprogress('ExecuteCalcFunction', 'ABS', b)
    assert b[2][2] == 9

    b = a.ExecutePythonScript(a.SCRIPTISSHARED, 'Capitalise.py$getNewString', 'Abc')
    printprogress('ExecutePythonScript', 'Capitalise.py$getNewString')
    assert b == 'abc'

    b = 'www.google.com'
    c = a.WebService(b)
    printprogress('WebService', c[:50])
    assert c.startswith('<!doctype html><html itemscope=')

    c = a.ExecuteBasicScript('', 'ScriptForge.SF_Utils._GetUNOService', 'FunctionAccess')
    b = a.UnoObjectType(c)
    printprogress('UnoObjectType', b)
    assert b == 'stardiv.StarCalc.ScFunctionAccess'
    c = a.ExecuteBasicScript('', 'ScriptForge.SF_Utils._GetUNOService', 'PathSettings')
    b = a.UnoObjectType(c)
    printprogress('UnoObjectType', b)
    assert b == 'com.sun.star.comp.framework.PathSettings'
    c = a.ExecuteBasicScript('', 'ScriptForge.SF_Utils._GetUNOService', 'ScriptProvider')
    b = a.UnoObjectType(c)
    printprogress('UnoObjectType', b)
    assert b == 'com.sun.star.script.provider.MasterScriptProvider'
    c = a.ExecuteBasicScript('', 'ScriptForge.SF_Utils._GetUNOService', 'SystemShellExecute')
    b = a.UnoObjectType(c)
    printprogress('UnoObjectType', b)
    assert b == 'com.sun.star.comp.system.SystemShellExecute' or b == 'com.sun.star.sys.shell.SystemShellExecute'
    c = a.ExecuteBasicScript('', 'ScriptForge.SF_Utils._GetUNOService', 'CoreReflection')
    b = a.UnoObjectType(c)
    printprogress('UnoObjectType', b)
    assert b == 'com.sun.star.comp.stoc.CoreReflection'
    c = uno.createUnoStruct("com.sun.star.beans.Property")
    b = a.UnoObjectType(c)
    printprogress('UnoObjectType', b)
    assert b == 'com.sun.star.beans.Property'

    c = a.ExecuteBasicScript('', 'ScriptForge.SF_Utils._GetUNOService', 'FunctionAccess')
    printprogress('HasUnoProperty')
    assert a.HasUnoProperty(c, 'Wildcards')

    printprogress('HasUnoMethod')
    assert a.HasUnoMethod(c, 'callFunction')

    printprogress('UnoProperties')
    assert 'Wildcards' in a.UnoProperties(c)

    printprogress('UnoMethods')
    assert 'callFunction' in a.UnoMethods(c)

    printprogress('SendMail')
    if userinteraction:
        a.SendMail('aaa@bbb.be', 'bbb@ccc.be, ddd@eee.eu', 'eee@fff.fr, fff@ggg.ch'
                   , filenames = '/home/jean-pierre/Documents/ScriptForge/Doc/Help/sf_timer.xhp'
                                  + ', /home/jean-pierre/Documents/ScriptForge/Doc/Help/sf_filesystem.xhp'
                   , subject = 'This is the subject'
                   , body = 'This is the body.' + '\n' + '2nd line'
                   , editmessage = True
                   )

    printprogress('RunCommand')
    if userinteraction:
        a.RunApplication('kate', '/home/jean-pierre/Install/install.txt')

    printprogress('OpenURLInBrowser')
    if userinteraction:
        a.OpenURLInBrowser('www.access2base.com')


# #####################################################################################################################
# TEST STRING                                                                                                       ###
# #####################################################################################################################
def testString():
    printprogress('TEST STRING')
    SF_String = CreateScriptService('String')


    a = '2019-12-31'
    b = SF_String.IsADate(a, 'YYYY-MM-DD')
    printprogress('IsADate', a, b)
    assert b

    c = "œ∑¡™£¢∞§¶•ªº–≠œ∑´®†¥¨ˆøπ“‘åß∂ƒ©˙∆˚¬"
    for d in ("MD5", "SHA1", "SHA224", "SHA256", "SHA384", "SHA512"):
        e = SF_String.HashStr(c, d)
        printprogress('HashStr', c, d, e)
        assert len(e) > 0 and (e == '616eb9c513ad07cd02924b4d285b9987' or d != 'MD5')

    a = "first.last@something.org"
    b = SF_String.IsEmail(a)
    printprogress('IsEmail', a, b)
    assert b

    printprogress('IsFileName')
    assert SF_String.IsFileName('C:\a\b\de f.ods', 'WINdows')
    assert SF_String.IsFileName('/home/my file.odt', 'LINUX')
    assert SF_String.IsFileName('/home/my file.odt')
    
    printprogress('IsIBAN')
    assert SF_String.IsIBAN('BE71 0961 2345 6769')
    assert SF_String.IsIBAN('BR15 0000 0000 0000 1093 2840 814 P2')
    assert SF_String.IsIBAN('FR76 3000 6000 0112 3456 7890 189')
    assert SF_String.IsIBAN('DE91 1000 0000 0123 4567 89')
    assert SF_String.IsIBAN('GR96 0810 0010 0000 0123 4567 890')
    assert SF_String.IsIBAN('MU43 BOMM 0101 1234 5678 9101 000 MUR')
    assert SF_String.IsIBAN('PK70 BANK 0000 1234 5678 9000')
    assert SF_String.IsIBAN('PL10 1050 0099 7603 1234 5678 9123')
    assert SF_String.IsIBAN('RO09 BCYP 0000 0012 3456 7890')
    assert SF_String.IsIBAN('LC14 BOSL 1234 5678 9012 3456 7890 1234')
    assert SF_String.IsIBAN('SA44 2000 0001 2345 6789 1234')
    assert SF_String.IsIBAN('ES79 2100 0813 6101 2345 6789')
    assert SF_String.IsIBAN('CH56 0483 5012 3456 7800 9')
    assert SF_String.IsIBAN('GB98 MIDL 0700 9312 3456 78')

    printprogress('IsIPv4')
    assert SF_String.IsIPv4("192.168.1.50")
    assert SF_String.isipv4("192.168.1.256") is False

    printprogress('IsLike')
    assert SF_String.IsLike('aAbB', '?A*')
    assert SF_String.IsLike('C:\\a\\b\\c\\f.odb', '?:*.*')

    printprogress('IsSheetName')
    assert SF_String.IsSheetName('1àbc + "def"')

    printprogress('IsUrl')
    assert SF_String.IsUrl('http://foo.bar/?q=Test%20URL-encoded%20stuff')
    assert SF_String.IsUrl('http//foo.bar/?q=Test%20URL-encoded%20stuff') is False

    printprogress('SplitNotQuoted')
    a = 'abc def ghi'  # returns ("abc", "def", "ghi")
    assert SF_String.SplitNotQuoted(a)[2] == 'ghi'
    a = 'abc,"def,ghi"'    #  returns ("abc", """def,ghi""")
    assert SF_String.SplitNotQuoted(a, ',')[1] == '"def,ghi"'
    a = 'abc,"def\\", ghi"'	# returns ("abc", """def \"", ghi""")
    assert SF_String.SplitNotQuoted(a, ',')[1] == '"def\\", ghi"'
    a = 'abc,"def\\,ghi"",'    # returns ("abc", """def\"",ghi""", "")
    assert SF_String.SplitNotQuoted(a, ',')[2] == ''
    assert len(SF_String.SplitNotQuoted(a, ',', 2)) == 2

    a = 'Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...'
    b = SF_String.Wrap(a, 20)
    assert b[5] == 'adipisci velit...'
    for c in b:
        printprogress('Wrap', c)

# #####################################################################################################################
# TEST TEXTSTREAM                                                                                                   ###
#######################################################################################################################
def testTextStream():
    printprogress('TEST TEXTSTREAM')
    a = ('E:\ScriptForge\Help\sf_filesystem.xhp' if bas.GetGuiType() == 1
         else '/home/jean-pierre/Documents/ScriptForge/Doc/Help/sf_filesystem.xhp')
    FSO = CreateScriptService("FileSystem")
    FSO.FileNaming = "SYS"
    b = FSO.GetTempName()
    FSO.CreateFolder(b)
    c = FSO.BuildPath(b, "stream.txt")
    FSO.CopyFile(a, c, overwrite = True)

    f = FSO.OpenTextFile(c, FSO.ForReading)
    printprogress('OpenTextFile', f.FileName, f.iomode, f.Encoding, f.newLine)
    assert f.IOMode == 'READ'

    while f.atEndOfStream is False:
        d = f.ReadLine()
    printprogress('ReadLine', f.Line)
    f.CloseFile()

    f = FSO.OpenTextFile(c, FSO.ForReading)
    f.SkipLine()
    f.SkipLine()
    d = f.ReadAll()
    printprogress('ReadAll', len(d))
    assert len(d) > 1000
    f.CloseFile()

    f = FSO.OpenTextFile(c, FSO.ForAppending)
    printprogress('WriteBlankLines')
    f.WriteBlankLines(5)
    f.writeline("Last line")
    f.CloseFile()
    f = FSO.OpenTextFile(c, FSO.ForReading)
    while f.AtEndOfStream is False:
        d = f.ReadLine()
    f.CloseFile()
    printprogress('WriteLine', d)
    assert d == 'Last line'

    c = FSO.BuildPath(b, "createstream.txt")
    f = FSO.CreateTextFile(c)
    h = 10
    for i in range(1, h + 1):
        f.WriteLine("Line #" + str(i))
    f.CloseFile()
    f = FSO.OpenTextFile(c)
    while f.AtEndOfStream is False:
        d = f.ReadLine()
    printprogress('CreateTextFile', f.Line, d)
    assert d.startswith('Line #') and f.Line == h

    for h in f.Properties():
        printprogress('GetProperty', h, f.GetProperty(h))
    assert f.SetProperty('NewLine', '\r\n')
    printprogress('SetProperty', "NewLine", f.NewLine)

    # printprogress('_repr', SF_String.Represent(f)[-50:])

    f.CloseFile()
    FSO.DeleteFolder(b)


# #####################################################################################################################
# TEST TIMER                                                                                                        ###
#######################################################################################################################
def testTimer():
    printprogress('TEST TIMER')
    import time
    a = CreateScriptService("Timer")
    a.start()
    time.sleep(0.2)
    a.suspend()
    printprogress('IsSuspended', a.issuspended, a.suspendduration)
    time.sleep(0.2)
    assert a.isSuspended
    a.Continue()
    for h in a.Properties():
        printprogress('GetProperty', h, a.getProperty(h))
    time.sleep(0.2)
    a.terminate()
    printprogress('Durations', a.Duration, a.SuspendDuration, a.TotalDuration)
    a.dispose()


# #####################################################################################################################
# TEST UI                                                                                                           ###
#######################################################################################################################
def testUI():
    printprogress('TEST UI')
    FSO = CreateScriptService("FileSystem")
    ui = CreateScriptService("UI")

    printprogress('Maximize, Minimize, Resize')
    ui.Activate('BASICIDE')
    ui.Minimize()
    ui.Maximize()
    ui.Resize()

    printprogress('CreateDocument')
    a = ui.CreateDocument('Calc')
    e = ui.CreateDocument('', FSO.BuildPath(FSO.TemplatesFolder, 'personal/CV.ott'))
    c = ui.ActiveWindow

    printprogress('CreateBaseDocument')
    dbf = FSO.BuildPath(FSO.TemporaryFolder, 'SF_XXX/abcd.odb')
    dba = ui.CreateBaseDocument(dbf, 'firebird')
    printprogress(ui.ActiveWindow)

    printprogress('Documents')
    b = ui.Documents()
    for i in range(len(b)):
        printprogress(str(i) + '. ', b[i])
    printprogress(c)
    assert c in b
    assert dbf in b
    dba.CloseDocument()

    printprogress('WindowExists')
    assert ui.WindowExists(c)

    printprogress("OpenDocument")
    f = FSO.BuildPath(FSO.TemporaryFolder, 'SF_XXX/FileA.ods')
    a.SaveAs(f, overwrite = False)
    a.SaveAs(f, overwrite = True)
    a.CloseDocument()
    a = ui.OpenDocument(f, readonly = True)
    a.CloseDocument()

    printprogress('OpenBaseDocument')
    dba = ui.OpenBaseDocument(dbf)

    printprogress('GetDocument')
    d = ui.GetDocument(c)
    assert d is not None
    dbd = ui.GetDocument('abcd.odb')
    assert dbd is not None

    printprogress('CloseDocument')
    d.CloseDocument()
    assert ui.WindowExists(c) is False
    a.CloseDocument()
    dbd.CloseDocument()
    assert ui.WindowExists('abcd.odb') is False

    FSO.DeleteFile(f)
    FSO.DeleteFile(dbf)

    printprogress('ActiveWindow', ui.ActiveWindow)

    printprogress('Dispose')
    ui = ui.Dispose()


# #####################################################################################################################
# TEST CALC                                                                                                         ###
#######################################################################################################################
def testCalc():
    printprogress('TEST CALC')
    FSO = CreateScriptService("FileSystem")
    UI = CreateScriptService("UI")
    SESS = CreateScriptService("Session")

    FSO.FileNaming = 'SYS'
    b = FSO.GetTempName()
    c = FSO.BuildPath(b, 'myDocumentA.ods')

    printprogress('CreateDocument')
    a = UI.CreateDocument('Calc')

    printprogress('SaveAs')
    a.SaveAs(c, overwrite = True)

    printprogress('InsertSheet')
    a.InsertSheet('NewSheet', 'Sheet1')
    assert 'NewSheet' in a.Sheets

    printprogress('Activate')
    a.Activate('NewSheet')
    d = a.Sheet('~')
    # assert d.SheetName == 'NewSheet'

    printprogress('CopySheet')
    a.CopySheet('NewSheet', 'NewNewSheet', 1)
    printprogress('List of sheets', a.Sheets)
    assert 'NewNewSheet' in a.Sheets
    e = UI.CreateDocument('Calc')
    a.CopySheet(e.Sheet('~'), 'SheetX', 1)
    #	Copy active sheet
    g = 'SheetX'    # e.Sheet('~').SheetName
    assert 'SheetX' in a.Sheets
    f = FSO.BuildPath(b, 'myDocumentE.ods')
    e.SaveAs(f, overwrite = True)
    e.CloseDocument()

    printprogress('CopySheetFromFile')
    a.CopySheetFromFile(f, g, 'SheetZZ')
    assert 'SheetZZ' in a.Sheets

    printprogress('GeColumnName')
    assert a.GetColumnName(1024) == 'AMJ'

    printprogress('MoveSheet')
    a.MoveSheet('SheetX')
    assert 'SheetX' in a.Sheets

    printprogress('RenameSheet')
    a.RenameSheet('SheetX', 'SheetZ')
    assert 'SheetZ' in a.Sheets

    printprogress('RemoveSheet')
    a.RemoveSheet('SheetZ')
    assert 'SheetZ' not in a.Sheets

    printprogress('SetValue, SetArray, GetValue')
    a.SetValue('SheetZZ.A1:D2', ((1, 2, 3, 4), (5, 6, 7, 8)))
    assert a.GetValue('SheetZZ.A1:D1')[3] == 4
    assert a.GetValue('SheetZZ.A1:D2')[1][3] == 8
    a.SetArray('SheetZZ.E1', tuple(range(1, 1001)))
    assert a.GetValue('SheetZZ.E1000') == 1000
    h = a.GetValue('SheetZZ.*')
    assert h[-1][-1] == 1000

    printprogress('Offset')
    h = a.Offset('SheetZZ.A1:D2', 999, 4, 1, 1)
    assert a.GetValue(h) == 1000

    printprogress('XSpreadsheet')
    assert SESS.UnoObjectType(a.XSpreadsheet('SheetZZ')) == 'ScTableSheetObj'

    printprogress('Height, Width')
    assert a.Height('SheetZZ.A1:Z100') == 100
    assert a.Width('SheetZZ.A1:Z100') == 26

    printprogress('XCellRange')
    assert SESS.UnoObjectType(a.XCellRange('SheetZZ.A1:Z100')) == 'ScCellRangeObj'

    printprogress('Last xxx', a.LastRow('SheetZZ'), a.LastColumn('SheetZZ'), a.LastCell('SheetZZ'))

    printprogress('CurrentSelection')
    a.CurrentSelection = 'SheetZZ.E1000'
    assert a.GetValue(a.CurrentSelection) == 1000
    a.CurrentSelection = ('SheetZZ.A1', 'SheetZZ.E1:E1000')
    print((';').join(a.CurrentSelection))
    assert (';').join(a.CurrentSelection) == '$SheetZZ.$A$1;$SheetZZ.$E$1:$E$1000'

    printprogress('CopyToCell')
    a.InsertSheet('SheetYY', 'SheetZZ')
    h = a.CopyToCell('SheetZZ.E1:E1000', 'SheetYY.B3:B5')
    assert h == '$SheetYY.$B$3:$B$1002'
    e = UI.CreateDocument('Calc')
    h = e.CopyToCell(a.Range('SheetZZ.E1:E1000'), '~.B3:B6')
    assert '$B$3:$B$1002' in h

    printprogress('CopyToRange')
    e.InsertSheet('SheetXYZ', 1)
    h = e.CopyToRange(a.Range('SheetZZ.E1:E1000'), 'SheetXYZ.A1:B1')
    assert ':$B$1000' in h
    e.CopyToRange('SheetXYZ.A3:B3', 'SheetXYZ.C3:E6')
    assert e.GetValue('SheetXYZ.E6') == 3

    printprogress('ClearValues/Formats/All')
    e.ClearFormats('SheetXYZ.*')
    e.ClearValues('SheetXYZ.*')
    e.ClearAll('SheetXYZ.*')

    printprogress('MoveRange')
    h = e.MoveRange(h, '~.D3:D6')
    assert '$D$3' in h
    e.CloseDocument(False)
    
    printprogress('ImportFromCSVFile')
    c = FSO.BuildPath(b, 'myCSVfile.csv')
    file = FSO.CreateTextFile(c, overwrite = True)
    file.WriteLine(
        'policyID,statecode,county,eq_site_limit,hu_site_limit,fl_site_limit,fr_site_limit,tiv_2011,tiv_2012,eq_site_deductible,hu_site_deductible,fl_site_deductible,fr_site_deductible,point_latitude,point_longitude,line,construction,point_granularity')
    file.WriteLine(
        '119736,FL,CLAY COUNTY,498960,498960,498960,498960,498960,792148.9,0,9979.2,0,0,30.102261,-81.711777,Residential,"Masonry",1')
    file.WriteLine(
        '448094,FL,CLAY COUNTY,1322376.3,1322376.3,1322376.3,1322376.3,1322376.3,1438163.57,0,0,0,0,30.063936,-81.707664,Residential,Masonry,3')
    file.WriteLine(
        '206893,FL,CLAY COUNTY,190724.4,190724.4,190724.4,190724.4,190724.4,192476.78,0,0,0,0,30.089579,-81.700455,Residential,Wood,2020/03/28')
    file.WriteLine(
        '333743,FL,CLAY COUNTY,0,79520.76,0,0,79520.76,86854.48,0,0,0,0,30.063236,-81.707703,Residential,Wood,3')
    file.WriteLine(
        '172534,FL,CLAY COUNTY,0,254281.5,0,254281.5,254281.5,246144.49,0,0,0,0,30.060614,-81.702675,Residential,Wood,1')
    file.WriteLine(
        '785275,FL,CLAY COUNTY,0,515035.62,0,0,515035.62,884419.17,0,0,0,0,30.063236,-81.707703,Residential,Masonry,3')
    file.WriteLine(
        '995932,FL,CLAY COUNTY,0,19260000,0,0,19260000,20610000,0,0,0,0,30.102226,-81.713882,Commercial,"""Reinforced"", and Concrete",1')
    file.WriteLine(
        '223488,FL,CLAY COUNTY,328500,328500,328500,328500,328500,348374.25,0,16425,0,0,30.102217,-81.707146,Residential,Wood,1')
    file.WriteLine(
        '433512,FL,CLAY COUNTY,315000,315000,315000,315000,315000,265821.57,0,15750,0,0,30.118774,-81.704613,Residential,Wood,1')
    file.WriteLine(
        '142071,FL,CLAY COUNTY,705600,705600,705600,705600,705600,1010842.56,14112,35280,0,0,30.100628,-81.703751,Residential,Masonry,1')
    file.WriteLine(
        "253816,FL,CLAY COUNTY,831498.3,831498.3,831498.3,831498.3,831498.3,1117791.48,0,0,0,0,30.10216,-81.719444,Residential,Masonry,1")
    file.CloseFile()
    g = 12  # g = FSO._CountTextLines(c)
    # PrintProgress("CSV: " & g & " lines")
    e = UI.CreateDocument("Calc")
    f = e.ImportFromCSVFile(c, 'C3')
    printprogress(f)
    assert e.GetValue(e.Offset('C3', 3, 17)) == '2020/03/28'
    assert e.GetValue(e.Offset('C3', 1, 16)) == 'Masonry'
    assert e.GetValue(e.Offset('C3', 7, 16)) == '"Reinforced", and Concrete'
    assert e.GetValue(e.Offset('C3', g - 1, 0)) == 253816

    printprogress('Get/SetFormula')
    e.SetFormula('B4', '=Sum(C4:T4')
    assert e.GetValue('B4') > 119736
    assert e.GetFormula('B4') == '=SUM(C4:T4)'
    e.SetFormula('B5:B6', ('=25-10', '=20-B5'))
    assert e.GetValue('B6') == 5
    e.SetFormula(e.Offset('B4', 0, 0, g - 1, 1), '=Sum(C4:T4')
    assert e.GetValue('B14') > 5000000

    printprogress('SortRange')
    f = e.SortRange('C3:T14', (3, 9), ('', 'DESC'), 'C20', containsheader = True)
    assert e.GetValue('T30') == '2020/03/28'
    printprogress(f)
    h = ['A' + str(i) for i in range(12)]
    e.SetArray('B20', h)
    f = e.SortRange('B20:T31', 1, 'DESC', containsheader = True, casesensitive = True)
    assert e.GetValue('B31') == 'A1'

    printprogress('SetCellStyle')
    e.SetCellStyle('B20:T20', 'Heading 2')
    e.SetCellStyle('B21:T31', 'Neutral')

    printprogress('DFunctions')
    printprogress('Avg = ', e.DAvg('B20:T31'))
    printprogress('Count = ', e.DCount('B20:T31'))
    printprogress('Max = ', e.DMax('B20:T31'))
    printprogress('Min = ', e.DMin('B20:T31'))
    printprogress('Sum = ', e.DSum('B20:T31'))
    assert e.DSum('B20:T31') / e.DCount('B20:T31') == e.DAvg('B20:T31')
    e.CloseDocument(False)

    printprogress('Create [Calc] BaseDocument')
    a.Save()
    a.CloseDocument()
    c = FSO.BuildPath(b, 'myDocumentA.ods')
    dba = ui.CreateBaseDocument(FSO.BuildPath(b, 'MyCalcDb.odb'), 'CALC', '', c)
    printprogress('Tables', dba.GetDatabase().Tables)
    dba.CloseDocument(False)
    dba = dba.Dispose()

    printprogress('Dispose')
    a = a.dispose()
    FSO.DeleteFolder(b)


# #####################################################################################################################
# TEST FODATABASE                                                                                                   ###
#######################################################################################################################
def testDatabase():
    printprogress('TEST DATABASE')
    import random as R
    FSO = CreateScriptService('FileSystem')
    FSO.filenaming = 'SYS'
    UI = CreateScriptService('UI')

    b = FSO.GetTempName()

    for db in ('HSQLDB', 'FIREBIRD'):
        printprogress(db, 'CreateBaseDocument')
        c = FSO.BuildPath(b, 'Test' + db + 'database.odb')
        a = UI.CreateBaseDocument(c, db)

        printprogress('GetDatabase')
        d = a.GetDatabase()

        printprogress('RunSql: Create a table')
        if db == 'HSQLDB':
            sql = "CREATE CACHED TABLE [EXPENSES]" + \
                  "([CATEGORY] VARCHAR(50),[VAT CODE] SMALLINT,[COMMENT] LONGVARCHAR,[EXPENSE DATE] TIMESTAMP(0)" + \
                  ",[DESCRIPTION] VARCHAR(50),[ID EXPENSE] INTEGER GENERATED BY DEFAULT AS IDENTITY(START WITH 0) NOT NULL PRIMARY KEY,[AMOUNT] DOUBLE)"
        elif db == 'FIREBIRD':
            sql = "CREATE TABLE [EXPENSES]" + \
                  "([CATEGORY] VARCHAR(50),[VAT CODE] SMALLINT,[COMMENT] VARCHAR(8000),[EXPENSE DATE] TIMESTAMP" + \
                  ",[DESCRIPTION] VARCHAR(50),[ID EXPENSE] INTEGER GENERATED BY DEFAULT AS IDENTITY PRIMARY KEY,[AMOUNT] DOUBLE PRECISION)"
        d.RunSql(sql, directsql = True)
        a.RunCommand('DBViewTables')
        a.RunCommand('DBRefreshTables')

        printprogress('RunSql: Store data')
        RecMax = 100
        for i in range(100):
            f = ('0000' + str(i + 1))[-4:]
            sql = "INSERT INTO [EXPENSES]([CATEGORY], [VAT CODE], [COMMENT], [EXPENSE DATE], [DESCRIPTION], [AMOUNT]) VALUES(" + "'CAT" + \
                  f + "'," + str(int(R.random() * 3 + 1)) + ",'" + "COMM" + f + "'," + "'2020-12-" + \
                  str(int(R.random() * 10) + 11) + "'," + "'DESC" + f + "'," + str(int(R.random() * 10000)) +\
                  ")"
            d.RunSql(sql)

        printprogress('GetRows')
        e = d.GetRows('EXPENSES', header = True)

        printprogress('GetProperty')
        h = d.Properties()
        for p in h:
            printprogress(p, d.GetProperty(p))

        printprogress('Close database')
        a.Save()
        printprogress('ThisComponent', bas.ThisComponent)
        printprogress('ThisDatabaseDocument', bas.ThisDatabaseDocument)
        a.CloseDocument(False)

        printprogress('Read data from outside db')
        d = CreateScriptService('Database', c, readonly = True)
        g = d.DCount('[ID EXPENSE]', 'EXPENSES')
        assert g == RecMax
        g = d.GetRows('EXPENSES', header = True)
        for i in range(len(g)):
            for j in range(len(g[0])):
                assert g[i][j] == e[i][j]

        d.closedatabase()

    printprogress('Calc.ImportFromDatabase')
    e = UI.CreateDocument('Calc')
    e.InsertSheet('Database')
    e.Activate('Database')
    printprogress('Import from registered db')
    f = '$Database.D1:E3'
    e.ImportFromDatabase('', 'Bibliography', f, 'biblio')
    printprogress('Import from table')
    f = '$Database.D25'
    e.ImportFromDatabase(c, '', f, 'EXPENSES')
    printprogress('Import from SQL')
    f = '$Database.D140'
    e.ImportFromDatabase(c, '', f, 'SELECT [CATEGORY], [AMOUNT] FROM [EXPENSES] ORDER BY [AMOUNT] DESC')

    e.CloseDocument(False)

    if bas.GetGuiType() != 1:
        FSO.DeleteFolder(b)  #	DoImport does not unlock source db in Windows ??


# #####################################################################################################################
# TEST DOCUMENT                                                                                                     ###
#######################################################################################################################
def testDocument():
    printprogress('TEST DOCUMENT')
    FSO = CreateScriptService("FileSystem")
    UI = CreateScriptService("UI")

    b = FSO.GetTempName()
    c = FSO.BuildPath(b, 'myDocument.ods')
    dbf = FSO.BuildPath(b, 'myBase1.odb')
    printprogress('CreateDocument')
    a = UI.CreateDocument('Calc')
    printprogress('CreateBaseDocument')
    dba = UI.CreateBaseDocument(dbf, 'firebird')

    printprogress('SaveAs')
    a.SaveAs(c, overwrite = True)
    dbf = dbf.replace('myBase1', 'myBase2')
    dba.SaveAs(dbf, overwrite = True)

    printprogress('DocumentProperties')
    a.Subject = 'This is the subject'
    a.Title = 'This is the title'
    a.Description = 'Line 01\nLine 02'
    a.Keywords = 'This, is, the, list, of, keywords'
    a.Save()
    a.closedocument()

    a = UI.OpenDocument(c, hidden = True)
    assert a.Subject == 'This is the subject'
    printprogress('Title', a.Title)
    for h in ('Subject', 'Title', 'Description', 'Keywords'):
        printprogress(h, a.GetProperty(h))
    g = a.Sheets
    for h in a.Properties():
        printprogress(h, a.GetProperty(h))

    printprogress('Activate')
    a.Activate()

    printprogress('SaveCopyAs')
    a.SaveCopyAs(FSO.BuildPath(b, 'myDocument2.ods'))
    a.Save()
    a.CloseDocument()
    dba.SaveCopyAs(dbf.replace('myBase2', 'myBase3'), overwrite = True)
    dba.CloseDocument()

    printprogress('Dispose')
    a = a.dispose()
    dba = dba.dispose()
    FSO.DeleteFolder(b)


# #####################################################################################################################
# TEST FORM                                                                                                         ###
#######################################################################################################################
def testForm():
    printprogress('TEST FORM')
    import time, datetime
    FSO = CreateScriptService('FileSystem')
    FSO.filenaming = 'SYS'
    UI = CreateScriptService('UI')
    UI.Activate('BASICIDE')
    SESS = CreateScriptService('Session')

    #	Test files are copied to temp directory
    tempdir = FSO.GetTempName()
    QADir = ( r'E:\ScriptForge\scriptforge\QA\QA Files' if bas.GetGuiType() == 1
              else '/home/jean-pierre/Documents/ScriptForge/Test/QA Files' )
    workdir = FSO.BuildPath(tempdir, 'FormsTests')
    FSO.CreateFolder(workdir)
    FSO.CopyFolder(QADir, workdir, overwrite = True)

    printprogress('Base form documents')
    a = UI.OpenBaseDocument(FSO.BuildPath(workdir, 'FB NorthWind.odb'), macroexecution = 1)
    printprogress('FormDocuments', ', '.join(a.FormDocuments()))

    printprogress('Base form document open')
    form = 'Folder1/Double forms'
    a.openFormDocument(form)
    b = a.Forms(form)
    printprogress('List forms', ', '.join(b))

    printprogress('Base form')
    c = a.Forms(form, 0)
    c.closeformdocument()

    printprogress('Form properties')
    a.OpenFormDocument('Subforms', False)
    b = a.Forms('Subforms', 0)
    c = b.Subforms('SubForm')
    d = c.Subforms('SubSubForm')
    f = 'vnd.sun.star.script:QA._Form.OnRecordChange?language=Basic&location=application'

    e = d.properties()
    printprogress('Set on-properties')
    for h in e:
        if h[0:2] == 'On':
            d.SetProperty(h, f)
    for h in e:
        printprogress(h, d.GetProperty(h))
    d.AllowDeletes = False
    d.AllowInserts = False
    d.AllowUpdates = False
    d.RecordSource = 'Order Details Copy'
    time.sleep(1)
    d.Filter = '[ProductID] > 100'
    time.sleep(1)
    d.Filter = ''
    time.sleep(1)
    d.OrderBy = '[ProductID] ASC'
    d.AllowDeletes = True
    d.AllowInserts = True
    d.AllowUpdates = True

    printprogress('Form methods')
    d.Requery()
    d.MoveLast()
    time.sleep(1)
    a = UI.GetDocument('FB NorthWind.odb')
    d.MoveFirst()
    time.sleep(1)
    d.MoveLast()
    time.sleep(1)
    d.MoveFirst()
    time.sleep(1)
    d.MoveNext(2)
    time.sleep(1)
    d.MovePrevious(2)

    b.CloseFormDocument()

    printprogress('Form control properties')
    form = 'AllTypes'
    a.OpenFormDocument(form)
    b = a.Forms(form, 0)
    d = list(b.Controls())
    d.sort()

    printprogress('READ properties (1)')
    for i in range(len(d)):
        e = b.Controls(d[i])
        printprogress(i, e.Name, e.ControlType)
        if len(e.Action) > 0:
            printprogress('', e.Name, ' ==> Action = ', e.Action)
        if len(e.Caption) > 0:
            printprogress('', e.Name, '==> Caption = ', e.Caption)
        if len(e.ControlSource) > 0:
            printprogress('', e.Name, '==> ControlSource = ', e.ControlSource)
        if e.ControlType == 'Button':
            printprogress('', e.Name, '==> Default = ', e.Default)
        if e.DefaultValue is not None:
            printprogress('', e.Name, '==> DefaultValue = ', e.DefaultValue)
        if e.ControlType != 'HiddenControl':
            printprogress('', e.Name, '==> Enabled = ', e.Enabled)
        if len(e.Format) > 0:
            printprogress('', e.Name, '==> Format = ', e.Format)
        if e.ListCount > 0:
            printprogress('', e.Name, '==> ListCount = ', e.ListCount)
        if e.ListIndex >= 0:
            printprogress('', e.Name, '==> ListIndex = ', e.ListIndex)
        if e.ControlType in ('ComboBox', 'ListBox'):
            printprogress('', e.Name, '==> Locked = ', e.Locked)
            printprogress("", e.Name, '==> MultiSelect = ', e.MultiSelect)
        g = e.Properties()
        for j in range(len(g)):
            if g[j][0:2] == 'On':
                h = e.GetProperty(g[j])
                if isinstance(h, str) and len(h) > 0:
                    printprogress('', e.Name, '==> ' + g[j] + " = ", h)
        if len(e.Picture) > 0:
            printprogress('', e.Name, '==> Picture = ', e.Picture)
        if SESS.HasUnoProperty(e.XControlModel, 'InputRequired'):
            printprogress('', e.Name, '==> Required = ', e.Required)
        if e.ControlType not in ('ComboBox', 'ListBox'):
            printprogress('', e.Name, '==> ListSource = ', e.ListSource)
        if e.ControlType not in ('ComboBox', 'ListBox'):
            printprogress('', e.Name, '==> ListSourceType = ', e.ListSourceType)
        if e.ControlType in ('ComboBox', 'DateField', 'FileControl', 'FormattedField', 
                             'PatternField', 'textField', 'TimeField'):
            printprogress('', e.Name, '==> Text = ', e.Text)
        if len(e.TipText) > 0:
            printprogress('', e.Name, '==> TipText = ', e.TipText)
        if e.ControlType == 'CheckBox':
            printprogress('', e.Name, '==> TripleState = ', e.TripleState)
        printprogress('', e.Name, '==> Value = ', e.Value)

    printprogress('MODIFY properties (1)')
    b.Controls('Push Button 1').Action = 'movetonew'
    b.Controls('Group Box 1').Caption = 'New Caption !!'
    b.Controls('Push Button 1').Default = True
    b.Controls('Text Box 1').Enabled = False
    b.Controls('Time Field 1').Format = '24h short'
    b.Controls('Combo Box 1').ListIndex = 10
    b.Controls('Combo Box 1').Locked = True
    b.Controls('List Box 1').MultiSelect = True
    b.Controls(
        'Text Box 1').OnMouseEntered = 'vnd.sun.star.script:QA._Form.OnControlEvent?language=Basic&location=application'
    b.Controls('Image Control 1').Picture = \
        '/home/jean-pierre/Documents/Access2Base/Doc/Access2Base/_wikiimages/tracelog dialog.png'
    b.Controls('Formatted Field 1').Required = True
    from com.sun.star.form.ListSourceType import TABLEFIELDS
    b.Controls('Combo Box 1').ListSourceType = TABLEFIELDS
    b.Controls('Combo Box 1').Locked = False
    b.Controls('Combo Box 1').ListSource = 'Products'
    b.Controls('Text Box 1').TipText = 'Special text here'
    b.Controls('Check Box 1').TripleState = True
    for i in range(5):
        b.Controls('List Box 1').Visible = False
        time.sleep(0.15)
        b.Controls('List Box 1').Visible = True
        time.sleep(0.15)

    printprogress('MODIFY Value property (1)')
    b.Controls('Check Box 1').Value = True
    b.Controls('Combo Box 1').Value = 'UnitsInStock'
    b.Controls('Currency Field 1').Value = 123.45
    b.Controls('Date Field 1').Value = datetime.datetime(2021, 1, 30)
    b.Controls('File Selection 1').Value = \
        '/home/jean-pierre/Documents/Access2Base/Doc/Access2Base/_wikiimages/tracelog dialog.png'
    b.Controls('Fixed Text 1').Caption = 'Other text'
    b.Controls('Formatted Field 1').Value = 'ABCD'
    b.Controls('List Box 1').MultiSelect = False
    b.Controls('List Box 1').Value = '92'
    b.Controls('Numeric Field 1').Value = b.Controls('Numeric Field 1').Value + 1
    b.Controls('Option Button 1').Value = True
    b.Controls('Option Button 2').Value = True
    b.Controls('Pattern Field 1').Value = 'VWXYZ'
    b.Controls('Scrollbar 1').Value = 50
    b.Controls('Spin Button 1').Value = 75
    b.Controls('Text Box 1').Value = 'Text in main text box'
    b.Controls('Time Field 1').Value = bas.Now()

    printprogress("Table control properties")
    c = b.Controls("Table Control  1")    #	2 spaces !?
    d = list(c.Controls())
    d.sort()

    printprogress('READ properties (2)', c.Name)
    for i in range(len(d)):
        e = c.Controls(d[i])
        printprogress(i, e.Name, e.ControlType)
        if len(e.Action) > 0:
            printprogress('', e.Name, ' ==> Action = ', e.Action)
        if len(e.Caption) > 0:
            printprogress('', e.Name, '==> Caption = ', e.Caption)
        if len(e.ControlSource) > 0:
            printprogress('', e.Name, '==> ControlSource = ', e.ControlSource)
        if e.ControlType == 'Button':
            printprogress('', e.Name, '==> Default = ', e.Default)
        if e.DefaultValue is not None:
            printprogress('', e.Name, '==> DefaultValue = ', e.DefaultValue)
        if e.ControlType != 'HiddenControl':
            printprogress('', e.Name, '==> Enabled = ', e.Enabled)
        if len(e.Format) > 0:
            printprogress('', e.Name, '==> Format = ', e.Format)
        if e.ListCount > 0:
            printprogress('', e.Name, '==> ListCount = ', e.ListCount)
        if e.ListIndex >= 0:
            printprogress('', e.Name, '==> ListIndex = ', e.ListIndex)
        if e.ControlType in ('ComboBox', 'ListBox'):
            printprogress('', e.Name, '==> Locked = ', e.Locked)
            printprogress("", e.Name, '==> MultiSelect = ', e.MultiSelect)
        g = e.Properties()
        for j in range(len(g)):
            if g[j][0:2] == 'On':
                h = e.GetProperty(g[j])
            if isinstance(h, str) and len(h) > 0:
                printprogress('', e.Name, '==> ' + g[j] + " = ", h)
        if len(e.Picture) > 0:
            printprogress('', e.Name, '==> Picture = ', e.Picture)
        if SESS.HasUnoProperty(e.XControlModel, 'InputRequired'):
            printprogress('', e.Name, '==> Required = ', e.Required)
        if e.ControlType in ('ComboBox', 'ListBox'):
            printprogress('', e.Name, '==> ListSource = ', e.ListSource)
        if e.ControlType in ('ComboBox', 'ListBox'):
            printprogress('', e.Name, '==> ListSourceType = ', e.ListSourceType)
        if e.ControlType in ('ComboBox', 'DateField', 'FileControl', 'FormattedField',
                             'PatternField', 'TextField', 'TimeField'):
            printprogress('', e.Name, '==> Text = ', e.Text)
        if len(e.TipText) > 0:
            printprogress('', e.Name, '==> TipText = ', e.TipText)
        if e.ControlType == 'CheckBox':
            printprogress('', e.Name, '==> TripleState = ', e.TripleState)
        printprogress('', e.Name, '==> Value = ', e.Value)

    printprogress('MODIFY properties (2)')
    c.Controls('Text Box 1').Enabled = False
    c.Controls('Time Field 1').Format = '24h short'
    c.Controls('Combo Box 1').ListIndex = 2
    c.Controls('Combo Box 1').Locked = True
    c.Controls('List Box 1').MultiSelect = True
    c.Controls(
        'Text Box 1').OnMouseEntered = 'vnd.sun.star.script:QA._Form.OnControlEvent?language=Basic&location=application'
    from com.sun.star.form.ListSourceType import TABLEFIELDS
    c.Controls('Combo Box 1').ListSourceType = TABLEFIELDS
    c.Controls('Combo Box 1').Locked = False
    c.Controls('Combo Box 1').ListSource = 'Products'
    c.Controls('Text Box 1').TipText = 'Special text here'
    c.Controls('Check Box 1').TripleState = True

    printprogress('MODIFY Value property (2)')
    c.Controls('Check Box 1').Value = True
    c.Controls('Combo Box 1').Value = 'UnitsInStock'
    c.Controls('Currency Field 1').Value = 123.45
    c.Controls('Date Field 1').Value = datetime.datetime(2021, 1, 30)
    c.Controls('List Box 1').MultiSelect = False
    c.Controls('Numeric Field 1').Value = c.Controls('Numeric Field 1').Value + 1
    c.Controls('Pattern Field 1').Value = 'VWXYZ'
    c.Controls('Text Box 1').Value = 'Text in table text box'
    c.Controls('Time Field 1').Value = bas.Now()

    e = c.Controls('Text Box 1')
    printprogress('', e.Name, '==> Value = ', e.Value)

    printprogress('SetFocus')
    c.Controls('Time Field 1').SetFocus()
    time.sleep(1)

    x = b.XForm
    # x.cancelRowUpdates()
    # b.CloseFormDocument()

    a.CloseDocument(False)
    FSO.DeleteFolder(tempdir)


# #####################################################################################################################
# TEST DIALOG & DIALOGCONTROL                                                                                       ###
#######################################################################################################################
def testDialog():
    printprogress('TEST DIALOG & DIALOGCONTROL')
    import time, datetime, uno
    FSO = CreateScriptService('FileSystem')
    FSO.filenaming = 'SYS'
    UI = CreateScriptService('UI')
    # UI.Activate('BASICIDE')
    BAS = CreateScriptService('Basic')
    
    a = CreateScriptService('Dialog', container = 'GlobalScope', library = 'QA', dialogname = 'QADialog')
    b = a.Execute(False)    #	Non-modal mode

    printprogress('TreeControl')
    c = a.Controls('TreeControl1')
    printprogress(c.XControlView)
    d = c.CreateRoot('ROOT 1')
    db = CreateScriptService('Database', '', 'Bibliography')
    e = 'SELECT [Custom1], [Author], [Title] FROM [biblio] ' + \
        'WHERE [Author] IS NOT NULL ORDER BY [Custom1], [Author], [Title]'
    f = db.GetRows(e)
    db.CloseDatabase()
    c.AddSubTree(d, f)
    c.XControlView.expandNode(d)
    
    printprogress('TreeControl/CurrentNode')
    c.CurrentNode = d

    printprogress('TreeControl/FindNode')
    c.CurrentNode = c.FindNode('*Sophie*', casesensitive = False)

    printprogress('Dialog Properties Get')
    for d in a.Properties():
        printprogress(a.Name, d, a.GetProperty(d))

    printprogress('Dialog Properties Setprintprogress')
    a.Caption = 'This is the title'
    printprogress(a.Name, 'Caption', a.Caption)
    he = a.Height
    a.Height = 400
    a.SetProperty('Height', 500)
    printprogress(a.Name, 'Height', a.Height)
    wi = a.Width
    a.Width = 500
    a.SetProperty('Width', 800)
    printprogress(a.Name, 'Width', a.Width)
    a.Height = he
    a.Width = wi
    a.Page = 2
    a.SetProperty('Page', 3)
    printprogress(a.Name, 'Page', a.Page)
    a.Page = 0
    a.Visible = False
    a.SetProperty('Visible', False)
    printprogress(a.Name, 'Visible', a.Visible)
    a.Visible = True
    printprogress(a.Name, 'OnMouseEntered', a.OnMouseEntered)

    printprogress('Tree control select & expand events')
    c = a.Controls('TreeControl1')
    c.OnNodeSelected = 'vnd.sun.star.script:QA._Dialog.NodeSelectedEvent?language=Basic&location=application'
    c.OnNodeExpanded = 'vnd.sun.star.script:QA._Dialog.NodeExpandedEvent?language=Basic&location=application'

    printprogress('Activate')
    ui.Activate('BASICIDE')
    a.Activate()

    printprogress('Controls')
    c = a.Controls()
    for i in range(len(c)):
        printprogress('Control #' + str(i), c[i])
    """printprogress('Controls Properties Get')
    for d in c:
        e = a.Controls(d)
        h = e.Properties()
        for f in h:
            if f not in ('CurrentNode', 'RootNode'):    # because they return UNO objects
                printprogress(e.Name, f, e.GetProperty(f))"""

    printprogress('Controls Properties Set')

    def setprops(val):
        for d in c:
            e = a.Controls(d)
            if e.ControlType in g or len(g) == 0:
                e.SetProperty(f, val)
                printprogress(e.Name, f, e.GetProperty(f))

    f = 'Cancel'
    g = ('Button',)
    setprops(True)
    
    f = 'Caption'
    g = ('Button', 'CheckBox', 'FixedLine', 'FixedText', 'GroupBox', 'RadioButton')
    setprops('New Label')
    
    f = 'Default'
    g = ('Button',)
    setprops(True)
    
    f = 'Enabled'
    g = ()
    setprops(False)
    setprops(True)
    
    f = 'Format'
    g = ('DateField',)
    setprops('MM/DD/YYYY')
    g = ('TimeField',)
    setprops('24h long')

    f = 'ListIndex'
    g = ('ComboBox', 'ListBox')
    for d in c:
        e = a.Controls(d)
        if e.ControlType in g or len(g) == 0:
            e.SetProperty(f, e.ListCount - 1)   # Select last
            printprogress(e.Name, f, e.GetProperty(f))

    f = 'Locked'
    g = ('ComboBox', 'CurrencyField', 'DateField', 'FileControl', 'FormattedField', 'ListBox',
                               'NumericField', 'PatternField', 'TextField', 'TimeField')
    setprops(True)
    setprops(False)

    f = 'MultiSelect'
    g = ('ListBox')
    setprops(True)

    f = 'OnKeyPressed'
    g = ()
    h = a.Controls('CommandButton1').OnActionPerformed
    setprops(h)
    setprops('')

    f = 'RowSource'
    g = ('ComboBox', 'ListBox')
    setprops(('MNO', 'PQR', 'STU', 'VWX', 'YZABC'))

    f = 'TipText'
    g = ()
    setprops('New tiptext !!')

    f = 'TripleState'
    g = ('CheckBox')
    setprops(True)

    f = 'Value'
    g = ('Button', 'CheckBox', 'ComboBox', 'CurrencyField', 'DateField', 'FileControl',
         'FormattedField', 'ListBox', 'NumericField', 'PatternField', 'ProgressBar', 'RadioButton', 'ScrollBar',
         'TextField', 'TimeField')
    for d in c:
        e = a.Controls(d)
        h = e.ControlType
        if h in g or len(g) == 0:
            if h in ('Button', 'CheckBox', 'RadioButton'):
                e.SetProperty(f, True)
            elif h in ('ComboBox', 'FileControl', 'PatternField', 'TextField'):
                e.SetProperty(f, 'YZABC')
            elif h in ('CurrencyField', 'FormattedField', 'NumericField', 'ProgressBar', 'ScrollBar'):
                e.SetProperty(f, 50)
            elif h in ('DateField', 'TimeField'):
                e.SetProperty(f, BAS.Now())
            elif h == 'ListBox':
                e.SetProperty(f, ('MNO', 'VWX'))
            printprogress(e.Name, f, e.GetProperty(f))

    f = 'Visible'
    g = ()
    setprops(False)
    setprops(True)

    a.Terminate()


def ControlEvent(poEvent):
    a = CreateScriptService("SFDialogs.DialogEvent", poEvent)
    if a is not None:
        exc.DebugPrint('Event triggered from Python in control', a.Name, 'of dialog', a.Parent.Name)


g_exportedScripts = (main, ControlEvent)
if __name__ == "__main__":
    main()
