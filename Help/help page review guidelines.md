- watch out filename convention
- replace Tab with 3 spaces
- replace 'LibreOffice' with %PRODUCTNAME
- add variable(link(literal))) in h1
- tablecontent(S) or tablecontent( )
- create 'abstract' section with introductory paragraphs
- create API bookmarks if necessary
- add Table of content for Methods, and methods' section
- set h3 to h2
- set h4 to h3
- inspect localize tag on h2, h3, h4 and bascode
- Embed sf_internalUse
- Add 'relatedTopics'

- remove leading space character before colon (:) signs
- review english for correctness
- no abbreviations in paragraphs/descriptions
- abbreviations are tolerated in arguments/parms descriptions

